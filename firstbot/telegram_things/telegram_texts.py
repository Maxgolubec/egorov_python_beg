WELCOME_MESSAGE = "\nЯ помогу найти места, где можно не только вкусно поесть, " \
                  "но и хорошо провести время!\n" \
                  "\nЧтобы получить список моих возможностей введи это: " + "/info\n" + \
                  "\nДля помощи и исправления багов обращайтесь к @cibilek\n"

# language=HTML
BOT_COMMANDS_MESSAGE = '<b>У меня есть следующие команды:</b>' \
                       '\n<b>/start - </b>начать все сначала.\n' \
                       '\n<b>/off - </b>остановить бота.\n' \
                       '\n<b>Для жалоб и предложений писать ему: @cibulek</b>'

STICKERS = {
    0: 'CAACAgIAAxkBAAECdb9g0t4pQ-GIFhN2VOQGWSlgGHLAfQACKgEAApNA9g58yKSycT1QbR8E',
    1: 'CAACAgIAAxkBAAECdcFg0t5BYyhb9GINDoSDZWiDDp_a_QACLQEAApNA9g7nmC7NdeTn_h8E',
    2: 'CAACAgIAAxkBAAECdcNg0t5QVggEQ9NYEgjnh8jZ_0FlbQACLwEAApNA9g7YMu7LCo3rvR8E',
    3: 'CAACAgIAAxkBAAECdcVg0t5kwE74XxHrWK5AikKGJWQAASkAAjABAAKTQPYOxJwvevoz3_0fBA',
    4: 'CAACAgIAAxkBAAECdcdg0t5xPnwbZ9FXaezfpO-my5BV0wACNAEAApNA9g5scsyR-A6kYh8E',
    5: 'CAACAgIAAxkBAAECdclg0t5_S8WM7auVJC4BjXjjlPAlrAACNgEAApNA9g4GzJuJvfMcjx8E',
    6: 'CAACAgIAAxkBAAECdctg0t6oEZ-k5TH3zDQ7oXhjvbi3KQACNwEAApNA9g68W9ith52j_R8E',
    7: 'CAACAgIAAxkBAAECdc1g0t7BdEP_LH4ctS3iBbCR8fk-kgACPAEAApNA9g64lXG9Vp9B2B8E',
    8: 'CAACAgIAAxkBAAECdc9g0t7T52CtNIBqPx4GVj5ASq4EvgACSgEAApNA9g6YNQY0Ioq46B8E'
}

UNCLEAR_MESSAGE = '<b>Я тебя не понял:(</b>\nПовтори, пожалуйста '

COMMAND_PHRASE = "\nПерекусим?" \
                 "\nВыбери интересующий район:"

COMMAND_PHRASE_BACK = "Выбери интересующий <b>район:</b>"

# language=HTML
INTENTION_MESSAGE = '<b>Цель визита?</b>'

# language=HTML
SOV_JUST_PLACE_LIST = '<b>Список заведений:</b>'\
                      '\n<b>!!!НАПРИМЕР!!!</b>\n'\
                      '\nЗаведение Номер1\n'\
                      '\nЗаведение Номер2\n'\
                      '\nЗаведение Номер3\n'

# language=HTML
SOV_COMPANY_FRIENDS_PLACE_LIST = '<b>Список заведений:</b>'\
                                 '\n<b>!!!НАПРИМЕР!!!</b>\n'\
                                 '\nЗаведение Номер4\n'\
                                 '\nЗаведение Номер5\n'\
                                 '\nЗаведение Номер6\n'

# language=HTML
SOV_RANDENEV_PLACE_LIST = '<b>Список заведений:</b>'\
                          '\n<b>!!!НАПРИМЕР!!!</b>\n'\
                          '\nЗаведение Номер7\n'\
                          '\nЗаведение Номер8\n'\
                          '\nЗаведение Номер9\n'

# language=HTML
NEW_SAVIN_JUST_PLACE_LIST_PAGE1 = '<b>Смотри, что нашел!</b>\n'\
                      '\n💈<b>Wаурма</b>, шаурмичная'\
                      '\n📌<b>Адрес:</b> Маршала Чуйкова 56/3\n'\
                      '\n<b>2ГИС</b> 3.5⭐'\
                      '\n<b>------------</b>\n'\
                      '\n💈<b>Lavanda cakes</b>, кофейня'\
                      '\n📌<b>Адрес:</b> Сибгата Хакима 41\n'\
                      '\n<b>2ГИС</b> 4.2⭐'\
                      '\n<b>Яндекс</b> 4.8⭐'\
                      '\n<b>------------</b>\n'\
                      '\n💈<b>P.Love</b>, Чайхана'\
                      '\n📌<b>Адрес:</b> Ямашева 97, ТЦ XL\n'\
                      '\n<b>2ГИС</b> 3.9⭐'\
                      '\n<b>Яндекс</b> 4.5⭐'\

NEW_SAVIN_JUST_PLACE_LIST_PAGE2 = '<b>Смотри, что нашел!</b>\n'\
                      '\n💈<b>1</b>, 2'\
                      '\n📌<b>Адрес:</b> 1\n'\
                      '\n<b>2ГИС</b> 0⭐'\
                      '\n<b>------------</b>\n'\
                      '\n💈<b>1</b>, 2'\
                      '\n📌<b>Адрес:</b> 1\n'\
                      '\n<b>2ГИС</b> 0⭐'\
                      '\n<b>Яндекс</b> 0⭐'\
                      '\n<b>------------</b>\n'\
                      '\n💈<b>1</b>, 2'\
                      '\n📌<b>Адрес:</b> 1\n'\
                      '\n<b>2ГИС</b> 0⭐'\
                      '\n<b>Яндекс</b> 0⭐'\

# language=HTML
NEW_SAVIN_COMPANY_FRIENDS_PLACE_LIST = '<b>Список заведений в Ново-Савиновском:</b>'\
                                 '\n<b>!!!НАПРИМЕР!!!</b>\n'\
                                 '\nЗаведение Номер4\n'\
                                 '\nЗаведение Номер5\n'\
                                 '\nЗаведение Номер6\n'

# language=HTML
NEW_SAVIN_RANDENEV_PLACE_LIST = '<b>Список заведений в Ново-Савиновском:</b>'\
                          '\n<b>!!!НАПРИМЕР!!!</b>\n'\
                          '\nЗаведение Номер7\n'\
                          '\nЗаведение Номер8\n'\
                          '\nЗаведение Номер9\n'

# language=HTML
VOLGA_MESSAGE = 'тут пока ничего нет<b>:)</b>'