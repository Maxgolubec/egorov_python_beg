from telebot import types, TeleBot
from dotenv import dotenv_values, find_dotenv

from telegram_things.telegram_markups import soviet_button_markup
from telegram_things.telegram_texts import SOV_JUST_PLACE_LIST, SOV_COMPANY_FRIENDS_PLACE_LIST, SOV_RANDENEV_PLACE_LIST

CONFIG = dotenv_values(find_dotenv(".env"))
TOKEN = CONFIG.get('TOKEN')
bot = TeleBot(TOKEN)


def just_eat(message):
    """Предлагаются места для ПРОСТОГО ПЕРЕКУСА"""
    bot.send_message(message.chat.id, SOV_JUST_PLACE_LIST, parse_mode='html',
                     reply_markup=soviet_button_markup())


def company_friends(message):
    """Предлагаются места для КОМПАНИИ ДРУЗЕЙ"""
    bot.send_message(message.chat.id, SOV_COMPANY_FRIENDS_PLACE_LIST, parse_mode='html',
                     reply_markup=soviet_button_markup())


def randenev(message):
    """Предлагаются места для СВИДАНИЯ"""
    bot.send_message(message.chat.id, SOV_RANDENEV_PLACE_LIST, parse_mode='html',
                     reply_markup=soviet_button_markup())