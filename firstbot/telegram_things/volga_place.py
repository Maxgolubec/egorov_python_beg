from telebot import types, TeleBot
from dotenv import dotenv_values, find_dotenv

from telegram_things.telegram_markups import volga_button_markup
from telegram_things.telegram_texts import VOLGA_MESSAGE

CONFIG = dotenv_values(find_dotenv(".env"))
TOKEN = CONFIG.get('TOKEN')
bot = TeleBot(TOKEN)

def just_eat_v(message):
    """Предлагаются места для ПРОСТОГО ПЕРЕКУСА"""
    bot.send_message(message.chat.id, VOLGA_MESSAGE, parse_mode='html',
                     reply_markup=volga_button_markup())


def company_friends_v(message):
    """Предлагаются места для КОМПАНИИ ДРУЗЕЙ"""
    bot.send_message(message.chat.id, VOLGA_MESSAGE, parse_mode='html',
                     reply_markup=volga_button_markup())


def randenev_v(message):
    """Предлагаются места для СВИДАНИЯ"""
    bot.send_message(message.chat.id, VOLGA_MESSAGE, parse_mode='html',
                     reply_markup=volga_button_markup())
