from telebot import types


def start_button_markup():
    """Создания кнопок на клавиатуре"""
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
    btn1 = types.KeyboardButton('🛣Советский🛣')
    btn2 = types.KeyboardButton('⛰Приволжский🏥')
    btn3 = types.KeyboardButton('🏙Ново-Савиновский🏙')
    btn4 = types.KeyboardButton('🏭Московский🏭')
    btn5 = types.KeyboardButton('🌲Кировский🌳')
    btn6 = types.KeyboardButton('🏰Вахитовский🏰')
    btn7 = types.KeyboardButton('✈Авиастроительный✈')
    markup.add(btn1, btn2, btn3, btn4, btn5, btn6, btn7)
    return markup


def soviet_button_markup():
    """Кнопки для работы с Советским районом"""
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
    just_eat = types.KeyboardButton('🛣Просто поесть🛣')
    company_friends = types.KeyboardButton('🛣Компания друзей🛣')
    randenev = types.KeyboardButton('🛣Свидание🛣')
    exit = types.KeyboardButton('Назад')
    markup.row(just_eat, company_friends, randenev)
    markup.row(exit)
    return markup


def volga_button_markup():
    """Кнопки для работы с Приволжским районом"""
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
    just_eat_sov = types.KeyboardButton('⛰Просто поесть🏥')
    company_friends_sov = types.KeyboardButton('⛰Компания друзей🏥')
    randenev_sov = types.KeyboardButton('⛰Свидание🏥')
    exit = types.KeyboardButton('Назад')
    markup.row(just_eat_sov, company_friends_sov, randenev_sov)
    markup.row(exit)
    return markup


def new_savin_button_markup():
    """Кнопки для работы с Ново-Савиновским районом"""
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
    just_eat_ns = types.KeyboardButton('🏙Просто поесть🏙')
    company_friends_ns = types.KeyboardButton('🏙Компания друзей🏙')
    randenev_ns = types.KeyboardButton('🏙Свидание🏙')
    exit = types.KeyboardButton('Назад')
    markup.row(just_eat_ns, company_friends_ns, randenev_ns)
    markup.row(exit)
    return markup


def moscow_button_markup():
    """Кнопки для работы с Московским районом"""
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
    just_eat = types.KeyboardButton('🏭Просто поесть🏭')
    company_friends = types.KeyboardButton('🏭Компания друзей🏭')
    randenev = types.KeyboardButton('🏭Свидание🏭')
    exit = types.KeyboardButton('Назад')
    markup.row(just_eat, company_friends, randenev)
    markup.row(exit)
    return markup


def kirov_button_markup():
    """Кнопки для работы с Кировским районом"""
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
    just_eat = types.KeyboardButton('🌲Просто поесть🌳')
    company_friends = types.KeyboardButton('🌲Компания друзей🌳')
    randenev = types.KeyboardButton('🌲Свидание🌳')
    exit = types.KeyboardButton('Назад')
    markup.row(just_eat, company_friends, randenev)
    markup.row(exit)
    return markup


def vakhitov_button_markup():
    """Кнопки для работы с Вахитовским районом"""
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
    just_eat = types.KeyboardButton('🏰Просто поесть🏰')
    company_friends = types.KeyboardButton('🏰Компания друзей🏰')
    randenev = types.KeyboardButton('🏰Свидание🏰')
    exit = types.KeyboardButton('Назад')
    markup.row(just_eat, company_friends, randenev)
    markup.row(exit)
    return markup


def aircraft_button_markup():
    """Кнопки для работы с Авиастроительным районом"""
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
    just_eat = types.KeyboardButton('✈Просто поесть✈')
    company_friends = types.KeyboardButton('✈Компания друзей✈')
    randenev = types.KeyboardButton('✈Свидание✈')
    exit = types.KeyboardButton('Назад')
    markup.row(just_eat, company_friends, randenev)
    markup.row(exit)
    return markup

# def next_back_page_button_markup():
#     markup = types.InlineKeyboardMarkup(row_width=2)
#     markup_next = types.InlineKeyboardMarkup('>>>')
#     markup_back = types.InlineKeyboardMarkup('<<<')
#     markup.add(markup_next, markup_back)
#     return markup

def back_to_markup():
    """Кнопка возврата к выбору района"""