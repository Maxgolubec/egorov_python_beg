import random

from dotenv import dotenv_values, find_dotenv
from telebot import types, TeleBot

from service import add_user_if_not_exist, soft_delete_user_by_id
from telegram_things.soviet_place import randenev, company_friends, just_eat
from telegram_things.new_savin_place import randenev_ns, company_friends_ns, just_eat_ns
from telegram_things.telegram_helpers import get_user_name, hi_mess
from telegram_things.telegram_markups import start_button_markup, soviet_button_markup, volga_button_markup, \
    new_savin_button_markup, moscow_button_markup, vakhitov_button_markup, kirov_button_markup, aircraft_button_markup
from telegram_things.telegram_texts import BOT_COMMANDS_MESSAGE, STICKERS, UNCLEAR_MESSAGE, \
    COMMAND_PHRASE, INTENTION_MESSAGE, SOV_JUST_PLACE_LIST, SOV_COMPANY_FRIENDS_PLACE_LIST, SOV_RANDENEV_PLACE_LIST,\
    COMMAND_PHRASE_BACK, NEW_SAVIN_JUST_PLACE_LIST_PAGE1, NEW_SAVIN_JUST_PLACE_LIST_PAGE2, NEW_SAVIN_COMPANY_FRIENDS_PLACE_LIST, NEW_SAVIN_RANDENEV_PLACE_LIST

CONFIG = dotenv_values(find_dotenv(".env"))
TOKEN = CONFIG.get('TOKEN')
bot = TeleBot(TOKEN)


def random_sticker(message):
    """Функция по генерации рандомного стикоса"""
    bot.send_sticker(message.chat.id, STICKERS[random.randrange(0, 8)],
                     reply_markup=start_button_markup())


def unclear_command(message):
    """При непонятной команде обращается к фразе в telegram_texts и кидает рандомный стикос"""
    random_sticker(message)
    bot.send_message(message.chat.id, UNCLEAR_MESSAGE, parse_mode='html',
                     reply_markup=start_button_markup())


def markup_edit_call(markup, message, call):
    bot.edit_message_text(message, call.from_user.id, call.message.message_id,
                          parse_mode='html', reply_markup=markup)


@bot.message_handler(commands=['start'])
def start(message):
    """Запуск бота"""
    name = get_user_name(message)
    add_user_if_not_exist(message.from_user.id, name)
    bot.send_message(message.chat.id, hi_mess(name), parse_mode='html')
    bot.send_message(message.chat.id, COMMAND_PHRASE, parse_mode='html',
                     reply_markup=start_button_markup())


@bot.message_handler(commands=['off'])
def off(message):
    """Остановка бота"""
    soft_delete_user_by_id(message.from_user.id)


@bot.message_handler(commands=['info'])
def info(message):
    """Вывод списка имеющихся команд"""
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    bot.send_message(message.chat.id, BOT_COMMANDS_MESSAGE, parse_mode='html',
                     reply_markup=start_button_markup())


@bot.message_handler(commands=['soviet'])
def soviet_work(message):
    """Предлагаются цели перекусить в Советском районе"""
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    bot.send_message(message.chat.id, INTENTION_MESSAGE, parse_mode='html',
                     reply_markup=soviet_button_markup())


@bot.message_handler(commands=['volga'])
def volga_work(message):
    """Предлагаются цели перекусить в Приволжском районе"""
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    bot.send_message(message.chat.id, INTENTION_MESSAGE, parse_mode='html',
                     reply_markup=volga_button_markup())


@bot.message_handler(commands=['new-savin'])
def new_savin_work(message):
    """Предлагаются цели перекусить в Ново-савиновском районе"""
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    bot.send_message(message.chat.id, INTENTION_MESSAGE, parse_mode='html',
                     reply_markup=new_savin_button_markup())


@bot.message_handler(commands=['moscow'])
def moscow_work(message):
    """Предлагаются цели перекусить в Московском районе"""
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    bot.send_message(message.chat.id, INTENTION_MESSAGE, parse_mode='html',
                     reply_markup=moscow_button_markup())


@bot.message_handler(commands=['kirov'])
def kirov_work(message):
    """Предлагаются цели перекусить в Кировском районе"""
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    bot.send_message(message.chat.id, INTENTION_MESSAGE, parse_mode='html',
                     reply_markup=kirov_button_markup())


@bot.message_handler(commands=['vakhitov'])
def vakhitov_work(message):
    """Предлагаются цели перекусить в Вахитовском районе"""
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    bot.send_message(message.chat.id, INTENTION_MESSAGE, parse_mode='html',
                     reply_markup=vakhitov_button_markup())


@bot.message_handler(commands=['aircraft'])
def aircraft_work(message):
    """Предлагаются цели перекусить в Авиастроительном районе"""
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    bot.send_message(message.chat.id, INTENTION_MESSAGE, parse_mode='html',
                     reply_markup=aircraft_button_markup())

# @bot.message_handler(commands=['next-page'])
# def next_page_ns_work(message):
#     """Перелистывание страниц заведений"""
#     add_user_if_not_exist(message.from_user.id, get_user_name(message))
#     bot.send_message(message.chat.id, NEW_SAVIN_JUST_PLACE_LIST_PAGE2, parse_mode='html',
#                     reply_markup=next_back_page_button_markup())


@bot.message_handler(commands=['back'])
def back_to_start_button_markup(message):
    """Возврат к выбору района"""
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    bot.send_message(message.chat.id, COMMAND_PHRASE_BACK, parse_mode='html',
                     reply_markup=start_button_markup())


# @bot.message_handler(commands=['back-page'])
# def back_page_ns_work(message):
#     """Перелистывание страниц заведений"""
#     add_user_if_not_exist(message.from_user.id, get_user_name(message))
#     bot.send_message(message.chat.id, reply_markup=new_savin_button_markup())


@bot.message_handler(commands=['s-je-list'])
def soviet_just_eat_place_list(message):
    """Показ заведений Советского района для Простого Перекуса"""
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    just_eat(message)


@bot.message_handler(commands=['s-cf-list'])
def soviet_company_friends_place_list(message):
    """Показ заведений Советского района для Компании Друзей"""
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    company_friends(message)


@bot.message_handler(commands=['s-r-list'])
def soviet_randenev_place_list(message):
    """Показ заведений Советского района для Свидания"""
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    randenev(message)

@bot.message_handler(commands=['ns-je-list'])
def new_savin_just_eat_place_list(message):
    """Показ заведений Ново-Савиновского района для Простого Перекуса"""
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    just_eat_ns(message)

@bot.message_handler(commands=['ns-cf-list'])
def new_savin_company_friends_place_list(message):
    """Показ заведений Ново-Савиновского района для Компании Друзей"""
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    company_friends_ns(message)

@bot.message_handler(commands=['ns-r-list'])
def new_savin_randenev_place_list(message):
    """Показ заведений Ново-Савиновского района для Свидания"""
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    randenev_ns(message)

@bot.message_handler(commands=['v-je-list'])
def volga_just_eat_place_list(message):
    """Показ заведений Приволжского района для Простого Перекуса"""
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    just_eat_v(message)

@bot.message_handler(commands=['v-cf-list'])
def volga_company_friends_place_list(message):
    """Показ заведений Приволжского района для Простого Перекуса"""
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    company_friends_v(message)

@bot.message_handler(commands=['v-r-list'])
def volga_randenev_list(message):
    """Показ заведений Приволжского района для Простого Перекуса"""
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    randenev_v(message)

@bot.message_handler(content_types=['text'])
def write_all(message):
    """Обработка клавиатурных кнопок"""
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    get_message_text = message.text.strip().lower()
    if get_message_text == '🛣советский🛣':
        soviet_work(message)
    elif get_message_text == '⛰приволжский🏥':
        volga_work(message)
    elif get_message_text == '🏙ново-савиновский🏙':
        new_savin_work(message)
    elif get_message_text == '🏭московский🏭':
        moscow_work(message)
    elif get_message_text == '🌲кировский🌳':
        kirov_work(message)
    elif get_message_text == '🏰вахитовский🏰':
        vakhitov_work(message)
    elif get_message_text == '✈авиастроительный✈':
        aircraft_work(message)
    elif get_message_text == 'назад':
        back_to_start_button_markup(message)
    elif get_message_text == '🛣просто поесть🛣':
        soviet_just_eat_place_list(message)
    elif get_message_text == '🛣компания друзей🛣':
        soviet_company_friends_place_list(message)
    elif get_message_text == '🛣свидание🛣':
        soviet_randenev_place_list(message)
    elif get_message_text == '🏙просто поесть🏙':
        new_savin_just_eat_place_list(message)
    elif get_message_text == '🏙компания друзей🏙':
        new_savin_company_friends_place_list(message)
    elif get_message_text == '🏙свидание🏙':
        new_savin_randenev_place_list(message)
    if get_message_text == '🏰просто поесть🏰':
        volga_just_eat_place_list(message)
    if get_message_text == '🏰компания друзей🏰':
        volga_company_friends_place_list(message)
    if get_message_text == '🏰свидание🏰':
        volga_randenev_list(message)
    # elif get_message_text == '>>>':
    #     next_page_ns_work(message)
    # elif get_message_text == '<<<':
    #     back_page_ns_work(message)
    # else:
    #     unclear_command(message)


@bot.message_handler(content_types='sticker')
def sticker(message):
    bot.send_sticker(message.chat.id, message.sticker.file_id, reply_markup=start_button_markup())
    bot.send_message(message.chat.id, "Я тоже так могу!!!", parse_mode='html', reply_markup=start_button_markup())


bot.polling(none_stop=True)