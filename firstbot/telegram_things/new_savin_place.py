from telebot import types, TeleBot
from dotenv import dotenv_values, find_dotenv

from telegram_things.telegram_markups import new_savin_button_markup
from telegram_things.telegram_texts import NEW_SAVIN_JUST_PLACE_LIST_PAGE1, NEW_SAVIN_JUST_PLACE_LIST_PAGE2, \
    NEW_SAVIN_COMPANY_FRIENDS_PLACE_LIST, NEW_SAVIN_RANDENEV_PLACE_LIST

CONFIG = dotenv_values(find_dotenv(".env"))
TOKEN = CONFIG.get('TOKEN')
bot = TeleBot(TOKEN)


def just_eat_ns(message):
    """Предлагаются места для ПРОСТОГО ПЕРЕКУСА"""
    bot.send_message(message.chat.id, NEW_SAVIN_JUST_PLACE_LIST_PAGE1, parse_mode='html',
                     reply_markup=new_savin_button_markup())


def company_friends_ns(message):
    """Предлагаются места для КОМПАНИИ ДРУЗЕЙ"""
    bot.send_message(message.chat.id, NEW_SAVIN_COMPANY_FRIENDS_PLACE_LIST, parse_mode='html',
                     reply_markup=new_savin_button_markup())


def randenev_ns(message):
    """Предлагаются места для СВИДАНИЯ"""
    bot.send_message(message.chat.id, NEW_SAVIN_RANDENEV_PLACE_LIST, parse_mode='html',
                     reply_markup=new_savin_button_markup())

def next_page_just_eat_ns(message):
    """Перелистывание страницы вперед в списке НС района просто перекусить"""
    bot.send_message(message.chat.id, NEW_SAVIN_RANDENEV_PLACE_LIST_PAGE2, parse_mode='html',
                     reply_markup=next_page_just_eat_ns())